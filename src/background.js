function genMp4VideosArray(data) {
    const mp4Videos = { presentation: [], other: [] };
    // Check if data is valid
    if (data.total == 1) {
        // Create an array of objects with (url of mp4, resolution of mp4)
        for (item of data.result[0].mediapackage.media.track) {
            if (item.url.endsWith('.mp4')) {
                if (item.type.includes("presentation")) {
                    mp4Videos.presentation.push({ url: item.url, res: item.video.resolution });
                } else {
                    mp4Videos.other.push({ url: item.url, res: item.video.resolution });
                }
            }
        }
    }
    return mp4Videos;
}

// Listener handler
function streamListener(requestDetails) {
    const filter = browser.webRequest.filterResponseData(requestDetails.requestId);
    const decoder = new TextDecoder("utf-8");

    const isMoodleTab = requestDetails.frameAncestors.length &&
        requestDetails.frameAncestors[0].url.includes("moodle.rwth-aachen.de");

    const data = [];

    const ocId = requestDetails.frameAncestors[0].url.includes("lti")
        ? null : new URL(requestDetails.url).searchParams.get("id");

    // Catch all data
    filter.ondata = (event) => {
        filter.write(event.data);

        if (isMoodleTab) {
            data.push(decoder.decode(event.data, { stream: true }));
        }
    }

    // When everything is received, forward everything to content script in Moodle tab
    filter.onstop = (_) => {
        filter.disconnect();

        if (isMoodleTab) {
            data.push(decoder.decode());
            const mp4Videos = genMp4VideosArray(JSON.parse(data.join("")));
            if (mp4Videos.presentation.length != 0 || mp4Videos.other.length != 0) {
                browser.tabs
                    .sendMessage(requestDetails.tabId, { ocId: ocId, data: mp4Videos })
                    .catch((err) => { console.error(err) });
            }
        }
    }
    return {};
}

// Add listener on video stream request
browser.webRequest.onBeforeRequest.addListener(
    streamListener,
    { urls: ["https://engage.streaming.rwth-aachen.de/search/episode.json?*"] },
    ["blocking"]
);
