function genDlLi(res, url) {
    const listElem = document.createElement("li");
    const link = document.createElement("a");
    link.appendChild(document.createTextNode(res));
    link.href = new URL(url);
    link.target = "_blank";
    listElem.appendChild(link);
    return listElem;
}

// Insert dl links in document
function insertDlUrls(ocId, mp4Videos) {
    // Do not add list if already in document
    if ((ocId != null && document.getElementById("dl_" + ocId) == null) ||
        (document.getElementsByClassName("dl-stream").length == 0)) {
        const list = document.createElement("ul");

        if (mp4Videos.presentation.length != 0) {
            const header = document.createElement("li");
            header.appendChild(document.createTextNode("Download stream:"));
            list.appendChild(header);
            for (video of mp4Videos.presentation) {
                list.appendChild(genDlLi(video.res, video.url));
            }
        }

        if (mp4Videos.other.length != 0) {
            const header = document.createElement("li");
            header.appendChild(document.createTextNode("Download secondary stream:"));
            list.appendChild(header);
            for (video of mp4Videos.other) {
                list.appendChild(genDlLi(video.res, video.url));
            }
        }

        // Append list ...
        const dlList = document.createElement("dl" + (ocId == null ? "" : "_" + ocId));
        dlList.classList.add("dl-stream");
        dlList.appendChild(list);
        if (ocId != null) {
            // ... below stream iframe
            Array
                .from(document.getElementsByClassName("ocplayer"))
                .find(el => el.src.includes(ocId)).parentElement.parentElement
                .appendChild(dlList);
        } else {
            // ... below Moodle activity header
            document.getElementsByClassName("activity-header")[0].appendChild(dlList);
        }
    }
}

// Add listener for background script messages
browser.runtime.onMessage.addListener((data) => {
    return Promise.resolve(insertDlUrls(data.ocId, data.data))
});
