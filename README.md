# Opencast stream downloader for RWTHmoodle
Webextension for downloading Opencast streams on RWTHmoodle

### [Releases](https://codeberg.org/antsa/opencast-dl-webext/releases)

# Installation
## Firefox:
Download the extension (.xpi file), drag the downloaded file into the browser window and click on `Add`.
## Chrome & Chromium-based:
Currently untested and probably incompatible! Please use Firefox instead.

